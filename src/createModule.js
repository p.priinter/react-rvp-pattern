const write = require('write')
const fs = require('fs')
const path = require('path')
const {TEMPLATE_KEY} = require('./template/constants')

class createClass {
  constructor() {
    this.scene = ''
    this.interactor = ''
    this.presenter = ''
    this.route = ''
    this.path = ''
    this.fileExtension = '.jsx'
  }
  init = ({scene, interactor, presenter, route, fileExtension = '.jsx'}) => {
    this.path = `${scene}/`
    this.scene = scene
    this.interactor = interactor
    this.presenter = presenter
    this.route = route
    this.fileExtension = fileExtension
  }
  getPath = fileName => {
    return `${this.path}${fileName}${this.fileExtension}`
  }
  replaceTemplate = file => {
    file = file.replace(TEMPLATE_KEY.VIEW, this.scene)
    file = file.replace(TEMPLATE_KEY.PRESENTER, this.presenter)
    file = file.replace(TEMPLATE_KEY.INTERACTOR, this.interactor)
    file = file.replace(TEMPLATE_KEY.ROUTE, this.route)
    return file
  }
  createScene = async () => {
    await this.createView()
    await this.createPresenter()
    await this.createInteractor()
    await this.createRoute()
  }
  createView = async () => {
    await fs.readFile(
      path.join(__dirname, '/template/view.jsx'),
      'utf8',
      (err, file) => {
        if (err) {
          console.error(err)
          return
        }
        file = this.replaceTemplate(file)
        write.sync(`${this.getPath(this.scene)}`, file, {newline: true})
      }
    )
  }
  createRoute = async () => {
    await fs.readFile(
      path.join(__dirname, '/template/route.jsx'),
      'utf8',
      (err, file) => {
        if (err) {
          console.error(err)
          return
        }
        file = this.replaceTemplate(file)
        write.sync(`${this.getPath(this.route)}`, file, {newline: true})
      }
    )
  }
  createPresenter = async () => {
    await fs.readFile(
      path.join(__dirname, '/template/presenter.jsx'),
      'utf8',
      (err, file) => {
        if (err) {
          console.error(err)
          return
        }
        file = this.replaceTemplate(file)
        write.sync(`${this.getPath(this.presenter)}`, file, {newline: true})
      }
    )
  }
  createInteractor = async () => {
    await fs.readFile(
      path.join(__dirname, '/template/interactor.jsx'),
      'utf8',
      (err, file) => {
        if (err) {
          console.error(err)
          return
        }
        file = this.replaceTemplate(file)
        write.sync(`${this.getPath(this.interactor)}`, file, {newline: true})
      }
    )
  }
}

const create = new createClass()
module.exports = {create}
