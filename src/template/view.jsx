import React, {useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import presenter from './__PRESENTER__'
import route from './__ROUTE__'

export const __VIEW__ = () => {
  const history = useHistory()

  useEffect(() => {
    route.history = history
  }, [])

  const some_function = e => {
    // do something
    presenter.doSomething(e)
  }

  return (
    <>
      <button onClick={some_function} />
    </>
  )
}
