const TEMPLATE_KEY = {
  VIEW: '__VIEW__',
  INTERACTOR: '__INTERACTOR__',
  PRESENTER: '__PRESENTER__',
  ROUTE: '__ROUTE__'
}

module.exports = {TEMPLATE_KEY}