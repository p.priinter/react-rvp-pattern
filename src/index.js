// ./lib/index.js
const prompt = require('prompt')
const colors = require('colors/safe')
const {create} = require('./createModule')
require('./extension')
/**
 * Displays a string in the console
 *
 * @param {string_to_say} String string to show in the console
 */

const init = async scenename => {
  prompt.message = ''
  prompt.delimiter = ''
  let {scene} = await prompt.get([
    {
      name: 'scene',
      description: 'Enter your scene name: ',
      pattern: /^\w+$/,
      required: true
    }
  ])
  scene = scene.toCapitalize()
  const {interactor, presenter, route} = await prompt.get([
    {
      name: 'interactor',
      description: 'Enter your Interactor name: ',
      default: `${scene}Interactor`,
      pattern: /^\w+$/,
      required: true
    },
    {
      name: 'presenter',
      description: 'Enter your Presenter name: ',
      default: `${scene}Presenter`,
      pattern: /^\w+$/,
      required: true
    },
    {
      name: 'route',
      description: 'Enter your Route name: ',
      default: `${scene}Route`,
      pattern: /^\w+$/,
      required: true
    }
  ])
  create.init({scene, interactor, presenter, route})
  create.createScene()
}

module.exports = {init}
