#!/usr/bin/env node
const yargs = require('yargs/yargs')
const {hideBin} = require('yargs/helpers')
const {init} = require('../src/index.js')

yargs(hideBin(process.argv))
  .option('name', {
    alias: 'n',
    type: 'string',
    description: 'name to create scene'
  })
  .command(
    'init [name]',
    'create the scene',
    yargs => {
      yargs.positional('name', {
        describe: 'name to create scene'
      })
    },
    argv => init(argv.name)
  ).argv
