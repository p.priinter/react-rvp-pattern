# React RVP Pattern
![npm](https://img.shields.io/npm/v/react-rvp-pattern)
![NPM](https://img.shields.io/npm/l/react-rvp-pattern)
## Install

Install with [npm](https://www.npmjs.com/) (requires [Node.js](https://nodejs.org/en/) >=10):


```sh
$ npm install rvp -g
```

## Usage

```sh
$ rvp init
```

### License

Copyright © 2021, [P. Pongsakorn](https://gitlab.com/p.priinter).
Released under the [MIT License](LICENSE).

***
