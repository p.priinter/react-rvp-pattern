import React, {useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import presenter from './OTPPresenter'
import route from './OTPRoute'

export const OTP = () => {
  const history = useHistory()

  useEffect(() => {
    route.history = history
  }, [])

  const some_function = e => {
    // do something
    presenter.doSomething(e)
  }

  return (
    <>
      <button onClick={some_function} />
    </>
  )
}
