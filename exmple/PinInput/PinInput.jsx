import React, {useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import presenter from './PinInputPresenter'
import route from './PinInputRoute'

export const PinInput = () => {
  const history = useHistory()

  useEffect(() => {
    route.history = history
  }, [])

  const some_function = e => {
    // do something
    presenter.doSomething(e)
  }

  return (
    <>
      <button onClick={some_function} />
    </>
  )
}
