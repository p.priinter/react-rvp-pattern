import interactor from './PinInputInteractor'
export default {
  //Call API
  callSomething({onSuccess, onError} = {}) {
    interactor.postSomething({
      onSuccess: resp => this.callSomethingSuccess(onSuccess, resp),
      onError: err => this.onErrorPostAuthenticate(onError, err)
    })
  },
  //Success Handler
  callSomethingSuccess(onSuccess = () => {}, resp) {
    //Handle onSuccess Case
    onSuccess(resp)
  },
  //Error Handler
  callSomethingError(onError = () => {}, resp) {
    //Handle onError Case
    onError(resp)
  },
  //Present
  doSomething(event) {
    //doSomething
    //return result
  }
}
