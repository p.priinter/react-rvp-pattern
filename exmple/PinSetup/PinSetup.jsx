import React, {useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import presenter from './PinSetUpPresenter'
import route from './PinSetUpRoute'

export const PinSetUp = () => {
  const history = useHistory()

  useEffect(() => {
    route.history = history
  }, [])

  const some_function = e => {
    // do something
    presenter.doSomething(e)
  }

  return (
    <>
      <button onClick={some_function} />
    </>
  )
}
